const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const APP_DIR = path.resolve(__dirname, "./src");
module.exports = function () {
  return {
    mode: "development",
    entry: "./src/index.jsx",
    output: {
      filename: "index.js",
      path: path.resolve(__dirname, "public"),
    },
    devServer: {
      static: {
        directory: path.join(__dirname, "public"),
      },
      port: 9000,
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: APP_DIR,
          exclude: /(node_modules|bower_components)/,
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: ["@babel/preset-env", "@babel/preset-react"], // 使用react语法时需要加上@babel/preset-react
                cacheCompression: false, // 默认值为 true。当设置此值时，会使用 Gzip 压缩每个 Babel transform 输出。如果你想要退出缓存压缩，将它设置为 false -- 如果你的项目中有数千个文件需要压缩转译，那么设置此选项可能会从中收益。
                cacheDirectory: true, // 转译结果缓存到文件系统中
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./public/index.html",
      }),
    ],
    resolve: {
      mainFiles: ["index"],
    },
  };
};
